<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Product::create([
            'category_id' => 1,
            'title' => 'Laptop',
            'description' => 'High-performance laptop',
            'sku' => 'ABCDE123',
            'price' => 999.99,
        ]);

        Product::create([
            'category_id' => 2,
            'title' => 'T-Shirt',
            'description' => 'Comfortable cotton t-shirt',
            'sku' => 'FGHIJ456',
            'price' => 19.99,
        ]);
    }
}
