<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(Product::with('category')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductRequest  $productRequest
     * @return JsonResponse
     */
    public function store(ProductRequest $productRequest): JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'message' => 'Product was created successful',
            'product' => Product::query()->create($productRequest->toArray())->load('category')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return JsonResponse
     */
    public function show(Product $product): JsonResponse
    {
        return response()->json($product->load('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductRequest  $productRequest
     * @param  Product $product
     * @return JsonResponse
     */
    public function update(ProductRequest $productRequest, Product $product): JsonResponse
    {
        $product->update($productRequest->toArray());

        return response()->json([
            'status' => 'success',
            'message' => 'Product was updated',
            'product' => $product->load('category')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product  $product
     * @return JsonResponse
     */
    public function destroy(Product $product): JsonResponse
    {
        $product->delete();

        return response()->json([
            'status' => 'success',
            'message' => $product->title . ' product was removed'
        ], 200);
    }
}
