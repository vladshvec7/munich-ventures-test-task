<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
      'title',
      'description'
    ];

    protected $casts = [
        'created_at' => "datetime:Y-m-d\TH:iPZ",
    ];

    /**
     * @return HasMany
     */
    public function products(): hasMany
    {
        return $this->hasMany(Product::class);
    }
}
